# README #

Check out from Develop and Sync that's it!

### What is this repository for? ###

Summary
PostBook Challenge App
Rest-API used to integrate: https://jsonplaceholder.typicode.com

GET	/posts/1 Returns a single json object so in this project GET /posts is used
And provision is made for GET /posts/{usedId} in PostBookService getPostsList
App Version 1.0

### How do I get set up? ###

Checkout from develop branch 
Sync gradle file no other configuration is need
Developed in Android studio 4.2.1
