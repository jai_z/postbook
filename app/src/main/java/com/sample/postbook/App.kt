package com.sample.postbook

import android.app.Application
import android.content.Context
import com.ww.roxie.Roxie
import di.DependencyInjection
import di.DependencyInjectionImplementation

open class App : Application() {
    open val di: DependencyInjection by lazy {
        DependencyInjectionImplementation(getString(R.string.app_url))
    }

    override fun onCreate() {
        super.onCreate()
        Roxie.enableLogging()
    }
}

val Context.di: DependencyInjection
    get() = (this.applicationContext as App).di

val Context.component: DependencyInjection
    get() = (this.applicationContext as App).component