package com.sample.postbook.postbook

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sample.postbook.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.postbook_layout_small.view.*
import model.PostBook

class PostBookListAdapter(
    private val listener: OnItemClickListener
) : ListAdapter<PostBook, PostBookListAdapter.PostBookListViewHolder>(
    DiffUtilCallback()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostBookListViewHolder {
        return PostBookListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                viewType,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PostBookListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.post_book_list_item
    }

    inner class PostBookListViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer, View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(postBook: PostBook) {
            containerView.title.text = postBook.title
            containerView.body.text = postBook.body

            containerView.fav_button.isSelected = postBook.isFavorite

            containerView.fav_button.setOnClickListener {
                if(postBook.isFavorite){
                    postBook.isFavorite = false
                    containerView.fav_button.isSelected = false
                    listener.onFavoriteClick(adapterPosition, postBook.isFavorite)
                }else{
                    postBook.isFavorite = true
                    containerView.fav_button.isSelected = true
                    listener.onFavoriteClick(adapterPosition, postBook.isFavorite)
                }

            }
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }

    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
        fun onFavoriteClick(position: Int, isFavorite: Boolean)
    }
}

private class DiffUtilCallback : DiffUtil.ItemCallback<PostBook>() {

    override fun areItemsTheSame(oldItem: PostBook, newItem: PostBook): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: PostBook, newItem: PostBook): Boolean =
        oldItem == newItem
}



