package com.sample.postbook.postbook

import model.PostBook

sealed class PostBookChange {
    object Loading: PostBookChange()
    data class PostLoaded(val postList: List<PostBook>): PostBookChange()
    object Error: PostBookChange()
}