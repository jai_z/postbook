package com.sample.postbook.postbook

import com.ww.roxie.BaseAction

sealed class PostBookAction: BaseAction {
    object LoadPosts: PostBookAction()
    data class PostClicked(val id: Int?) : PostBookAction()
}