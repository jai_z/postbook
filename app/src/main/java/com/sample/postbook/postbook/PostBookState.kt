package com.sample.postbook.postbook

import com.ww.roxie.BaseState
import model.PostBook

data class PostBookState(
    val loading: Boolean = false,
    val postBook: List<PostBook>? = null,
    val isError: Boolean = false
): BaseState