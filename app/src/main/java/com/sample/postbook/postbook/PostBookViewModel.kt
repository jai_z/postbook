package com.sample.postbook.postbook

import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers

class PostBookViewModel(PostBookState: PostBookState?,
                        private val postBookUseCase: PostBookUseCase
) :
    BaseViewModel<PostBookAction, PostBookState>() {
    override val initialState =  PostBookState ?: PostBookState(loading = false)

    private val reducer: Reducer<PostBookState, PostBookChange> = { state, change ->
        when (change) {
            is PostBookChange.Loading -> state.copy(loading = true, isError = false)
            is PostBookChange.PostLoaded -> state.copy(
                loading = false,
                postBook = change.postList,
                isError = false
            )
            is PostBookChange.Error -> state.copy(loading = false, isError = true)
        }
    }

    init {
        bindActions()
    }

    private fun bindActions() {
        val getPostChange = actions.ofType(PostBookAction.LoadPosts::class.java)
            .switchMap {
                postBookUseCase.getPostList()
                    .subscribeOn(Schedulers.io())
                    .toObservable()
                    .map<PostBookChange> { post ->
                        PostBookChange.PostLoaded(post)
                    }
                    .onErrorReturn { PostBookChange.Error }
                    .startWith(PostBookChange.Loading)
            }
        disposables += getPostChange.scan(initialState, reducer)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue)
    }
}