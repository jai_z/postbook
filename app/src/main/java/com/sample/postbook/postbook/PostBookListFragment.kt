package com.sample.postbook.postbook

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.sample.postbook.R
import com.sample.postbook.di
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.post_book_list_fragment.*
import kotlinx.android.synthetic.main.post_book_list_fragment.progressBar
import model.PostBook

class PostBookListFragment : Fragment(R.layout.post_book_list_fragment), PostBookListAdapter.OnItemClickListener {

    //Not used the args
    //private val args: PostBookListFragmentArgs by navArgs()

    private val adapter =  PostBookListAdapter(this)

    private var postBook: List<PostBook>? = null
    private var favoritePostBook: List<PostBook>? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerView.layoutManager = LinearLayoutManager(context)

        val postBookViewModel = (activity)?.di?.postBookViewModel
        postBookViewModel?.observableState?.observe(  this, Observer { state ->
            renderState(state)
        })

        postBookViewModel?.dispatch(PostBookAction.LoadPosts)

        allButton.setOnClickListener {
            adapter.submitList(postBook)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()
            allButton.isSelected = true
            favoriteButton.isSelected = false
        }

        favoriteButton.setOnClickListener {
            adapter.submitList(favoritePostBook)
            recyclerView.adapter = adapter
            adapter.notifyDataSetChanged()
            favoriteButton.isSelected = true
            allButton.isSelected = false
        }

    }

    private fun renderState(state: PostBookState) {
        with(state){
            progressBar.isVisible = loading
            adapter.submitList(postBook)
            recyclerView.adapter = adapter
            this@PostBookListFragment.postBook = postBook
            allButton.isSelected = true
        }
    }


    override fun onItemClick(position: Int) {
        PostBookAction.PostClicked(this@PostBookListFragment.postBook?.get(position)?.id)
        if(postBook!=null){
            val action = PostBookListFragmentDirections.actionPostListFragmentToPostDetailsFragment(
                postBook!![position]
            )
            findNavController().navigate(action)
        }
    }

    override fun onFavoriteClick(position: Int, isFavorite: Boolean) {
        this@PostBookListFragment.postBook?.get(position)?.isFavorite = isFavorite
        favoritePostBook = this@PostBookListFragment.postBook?.filter {
            it.isFavorite
        } ?: emptyList()
    }


}