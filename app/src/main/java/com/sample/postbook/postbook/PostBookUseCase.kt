package com.sample.postbook.postbook

import io.reactivex.Single
import model.PostBook
import model.PostBookRepository

interface PostBookUseCase {
    fun getPostList():  Single<List<PostBook>>
}

class PostBookUseCaseImplementation(private val postBookRepository: PostBookRepository): PostBookUseCase {
    override fun getPostList(): Single<List<PostBook>> {
        return postBookRepository.getPosts()
    }
}