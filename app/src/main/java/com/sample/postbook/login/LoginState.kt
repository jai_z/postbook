package com.sample.postbook.login

import com.ww.roxie.BaseState

data class LoginState (
    val loading: Boolean = false,
    val isUserIdSuccess: Boolean = false,
    val isError: Boolean = false
): BaseState