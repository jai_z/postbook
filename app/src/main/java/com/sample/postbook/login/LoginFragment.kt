package com.sample.postbook.login

import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sample.postbook.R
import com.sample.postbook.utils.Utils
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : Fragment(R.layout.login_fragment) {

    override fun onStart() {
        super.onStart()
        loginButton.setOnClickListener {
            LoginAction.LoginButtonClicked
            if(Utils.isValidNumber(userIdEditText.text.toString())){
                val action = LoginFragmentDirections.actionLoginFragmentToPostListFragment(userIdEditText.text.toString())
                findNavController().navigate(action)
                LoginChange.LoginSuccess
                activity?.let { it1 -> Utils.hideKeyboard(it1) }
            }else{
                errorTextView.visibility = View.VISIBLE
                LoginChange.Error
            }
        }
    }

}