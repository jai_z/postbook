package com.sample.postbook.login


sealed class LoginChange {
    object LoginSuccess: LoginChange()
    object Error: LoginChange()
}