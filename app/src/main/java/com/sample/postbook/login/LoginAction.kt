package com.sample.postbook.login

import com.ww.roxie.BaseAction

sealed class LoginAction: BaseAction {
    object  LoginButtonClicked: LoginAction()
}
