package com.sample.postbook.postbookdetails

import io.reactivex.Single
import model.Comments
import model.PostBookRepository

interface CommentsUseCase {
    fun getCommentsList(postId: Int): Single<List<Comments>>
}

class CommentsUseCaseImplementation(private val postBookRepository: PostBookRepository):
    CommentsUseCase {

    override fun getCommentsList(postId: Int): Single<List<Comments>> {
        return postBookRepository.getComments(postId)
    }
}