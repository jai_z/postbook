package com.sample.postbook.postbookdetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sample.postbook.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.comment_layout_small.view.*
import model.Comments

class CommentsAdapter( private val listener: OnItemClickListener
) : ListAdapter<Comments, CommentsAdapter.PostBookListViewHolder>(
    DiffUtilCallback()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostBookListViewHolder {
        return PostBookListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                viewType,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PostBookListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.comment_list_item
    }

    inner class PostBookListViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer, View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(comment: Comments) {
            //Not used context
            //val context = containerView.context
            containerView.userName.text = comment.name
            containerView.body.text = comment.body
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}

private class DiffUtilCallback : DiffUtil.ItemCallback<Comments>() {

    override fun areItemsTheSame(oldItem: Comments, newItem: Comments): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: Comments, newItem: Comments): Boolean =
        oldItem == newItem
}