package com.sample.postbook.postbookdetails

import model.Comments

sealed class  CommentsChange {
    object Loading: CommentsChange()
    data class CommentsLoaded(val commentsList: List<Comments>): CommentsChange()
    object Error: CommentsChange()
}