package com.sample.postbook.postbookdetails

import com.ww.roxie.BaseState
import model.Comments

data class CommentsState(
    val loading: Boolean = false,
    val comments: List<Comments>? = null,
    val isError: Boolean = false
) : BaseState