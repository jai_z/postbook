package com.sample.postbook.postbookdetails

import com.ww.roxie.BaseAction

sealed class CommentsAction: BaseAction {
    object LoadComments: CommentsAction()
    data class LoadCommentsWithId(val postId :Int): CommentsAction()
}