package com.sample.postbook.postbookdetails

import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.sample.postbook.R
import com.sample.postbook.di
import kotlinx.android.synthetic.main.post_book_list_fragment.*
import kotlinx.android.synthetic.main.postbook_layout_small.*
import model.Comments

class PostDetailsFragment : Fragment(R.layout.post_book_details_fragment), CommentsAdapter.OnItemClickListener {

    private val args: PostDetailsFragmentArgs by navArgs()

    private val adapter =  CommentsAdapter(this)
    private var commentsList: List<Comments>? = null

    override fun onResume() {
        super.onResume()

        title.text = args.postBook.title
        body.text = args.postBook.body
        fav_button.isSelected= args.postBook.isFavorite

        recyclerView.layoutManager = LinearLayoutManager(context)

        val commentsViewModel = (activity)?.di?.commentsViewModel

        commentsViewModel?.observableState?.observe(  this, Observer { state ->
            renderState(state)
        })

        commentsViewModel?.dispatch(CommentsAction.LoadCommentsWithId(args.postBook.id))

    }

    private fun renderState(state: CommentsState) {
        with(state){
            progressBar.isVisible = loading
            adapter.submitList(comments)
            recyclerView.adapter = adapter

            this@PostDetailsFragment.commentsList = comments
        }
    }

    override fun onItemClick(position: Int) {
        //TODO("Not yet implemented")
    }

}