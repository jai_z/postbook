package com.sample.postbook.postbookdetails

import com.ww.roxie.BaseViewModel
import com.ww.roxie.Reducer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers

class CommentsViewModel(commentsState: CommentsState?,
                        private val commentsUseCase: CommentsUseCase
) :
    BaseViewModel<CommentsAction, CommentsState>() {
    override val initialState =  commentsState ?: CommentsState(loading = false)

    private val reducer: Reducer<CommentsState, CommentsChange> = { state, change ->
        when (change) {
            is CommentsChange.Loading -> state.copy(loading = true, isError = false)
            is CommentsChange.CommentsLoaded -> state.copy(
                loading = false,
                comments = change.commentsList,
                isError = false
            )
            is CommentsChange.Error -> state.copy(loading = false, isError = true)
        }
    }

    init {
        bindActions()
    }

    private fun bindActions() {
        val getPostChange = actions.ofType(CommentsAction.LoadCommentsWithId::class.java)
            .switchMap {
                commentsUseCase.getCommentsList(it.postId)
                    .subscribeOn(Schedulers.io())
                    .toObservable()
                    .map<CommentsChange> { post ->
                        CommentsChange.CommentsLoaded(post)
                    }
                    .onErrorReturn { CommentsChange.Error }
                    .startWith(CommentsChange.Loading)
            }
        disposables += getPostChange.scan(initialState, reducer)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue)
    }
}