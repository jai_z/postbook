package model

import model.api.PostBookService
import io.reactivex.Single
import java.lang.Exception
import kotlin.random.Random

class PostBookRepository(private val postBookService: PostBookService) {

    @Throws(Exception::class)
    fun getPosts(): Single<List<PostBook>>{
        return postBookService.getPosts()
    }

    @Throws(Exception::class)
    fun getComments(postId: Int): Single<List<Comments>>{
        return postBookService.getComments(postId)
    }

}