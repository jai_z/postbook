package model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostBook(
    val userId : Int,
    val title: String,
    val id: Int,
    val body: String,
    var isFavorite: Boolean = false
): Parcelable