package model.api

import io.reactivex.Single
import model.Comments
import model.PostBook
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostBookService {
    @GET("posts/{id}")
    fun getPostsList(@Path("id") userID: String): Single<List<PostBook>>

    @GET("posts")
    fun getPosts(): Single<List<PostBook>>

    @GET("posts/{id}/comments")
    fun getComments(@Path("id") postId: Int): Single<List<Comments>>
}