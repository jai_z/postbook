package di

import com.sample.postbook.login.LoginAction
import com.sample.postbook.login.LoginState
import com.sample.postbook.postbook.PostBookAction
import com.sample.postbook.postbook.PostBookState
import com.sample.postbook.postbook.PostBookUseCaseImplementation
import com.sample.postbook.postbook.PostBookViewModel
import com.sample.postbook.postbookdetails.CommentsAction
import com.sample.postbook.postbookdetails.CommentsState
import com.sample.postbook.postbookdetails.CommentsUseCaseImplementation
import com.sample.postbook.postbookdetails.CommentsViewModel
import com.squareup.moshi.Moshi
import com.ww.roxie.BaseViewModel
import model.PostBookRepository
import model.api.PostBookService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class DependencyInjectionImplementation(apiUrl: String) : DependencyInjection {
    override lateinit var postBookViewModel: BaseViewModel<PostBookAction, PostBookState>
    override lateinit var commentsViewModel: BaseViewModel<CommentsAction, CommentsState>
    override lateinit var loginViewModel: BaseViewModel<LoginAction, LoginState>

    init {
        val moshi = Moshi.Builder().build()

        val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(apiUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        val postBookService = retrofit.create(PostBookService::class.java)
        val postBookRepository = PostBookRepository(postBookService)

        val postBookUseCase = PostBookUseCaseImplementation(postBookRepository)
        postBookViewModel = PostBookViewModel(PostBookState(loading = false), postBookUseCase)

        val commentsUseCase = CommentsUseCaseImplementation(postBookRepository)
        commentsViewModel = CommentsViewModel(CommentsState(loading = false), commentsUseCase)

    }
}