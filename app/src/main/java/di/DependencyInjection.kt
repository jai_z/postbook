package di

import com.sample.postbook.login.LoginAction
import com.sample.postbook.login.LoginState
import com.sample.postbook.postbook.PostBookAction
import com.sample.postbook.postbook.PostBookState
import com.sample.postbook.postbookdetails.CommentsAction
import com.sample.postbook.postbookdetails.CommentsState
import com.ww.roxie.BaseViewModel

interface DependencyInjection{
    val postBookViewModel: BaseViewModel<PostBookAction, PostBookState>
    val commentsViewModel: BaseViewModel<CommentsAction, CommentsState>
    val loginViewModel: BaseViewModel<LoginAction, LoginState>
}