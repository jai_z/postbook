package com.sample.postbook

import com.sample.postbook.postbook.PostBookAction
import com.sample.postbook.postbook.PostBookState
import com.ww.roxie.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.TestObserver
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject

class AndroidTestViewModel : BaseViewModel<PostBookAction, PostBookState>() {

    private val testAction = TestObserver<PostBookAction>()
    private val testState = PublishSubject.create<PostBookState>()

    override val initialState = PostBookState()

    init {
        actions.subscribe(testAction)
        disposables += testState
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(state::setValue)
    }
}