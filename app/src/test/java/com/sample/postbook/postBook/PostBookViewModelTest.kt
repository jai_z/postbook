package com.sample.postbook.postBook

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.sample.postbook.RxTestSchedulerRule
import com.sample.postbook.postbook.PostBookAction
import com.sample.postbook.postbook.PostBookState
import com.sample.postbook.postbook.PostBookUseCase
import com.sample.postbook.postbook.PostBookViewModel
import io.reactivex.Single
import model.PostBook
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class PostBookViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testRule: RxTestSchedulerRule = RxTestSchedulerRule()

    private lateinit var testSubject: PostBookViewModel

    private val initialState = PostBookState(loading = false)

    private val loadingState = PostBookState(loading = true)

    private val postBookUseCase = mock<PostBookUseCase>()

    private val observer = mock<Observer<PostBookState>>()

    @Before
    fun setUp() {
        testSubject = PostBookViewModel(initialState, postBookUseCase)
        testSubject.observableState.observeForever(observer)
    }

    @Test
    fun `Given post successfully loaded, when action GetPost is received, then State contains post`() {

        val postList: List<PostBook> = emptyList()
        val post = PostBook(1, "test title", 1, "test body", false)
        postList.plus(post)

        val successState = PostBookState(loading = false, postBook = postList)

        whenever(postBookUseCase.getPostList()).thenReturn(Single.just(postList))

        testSubject.dispatch(PostBookAction.LoadPosts)
        testRule.triggerActions()

        inOrder(observer) {
            verify(observer).onChanged(initialState)
            verify(observer).onChanged(loadingState)
            verify(observer).onChanged(successState)
        }

        verifyNoMoreInteractions(observer)
    }

    @Test
    fun `Given post failed to load, when action GetPost received, then State contains error`() {
        whenever(postBookUseCase.getPostList()).thenReturn(Single.error(RuntimeException()))
        val errorState = PostBookState(isError = true)

        testSubject.dispatch(PostBookAction.LoadPosts)
        testRule.triggerActions()

        inOrder(observer) {
            verify(observer).onChanged(initialState)
            verify(observer).onChanged(loadingState)
            verify(observer).onChanged(errorState)
        }

        verifyNoMoreInteractions(observer)
    }


}