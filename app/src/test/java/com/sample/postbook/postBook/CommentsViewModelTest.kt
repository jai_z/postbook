package com.sample.postbook.postBook

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.sample.postbook.RxTestSchedulerRule
import com.sample.postbook.postbookdetails.CommentsAction
import com.sample.postbook.postbookdetails.CommentsState
import com.sample.postbook.postbookdetails.CommentsUseCase
import com.sample.postbook.postbookdetails.CommentsViewModel
import io.reactivex.Single
import model.Comments
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CommentsViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val testRule: RxTestSchedulerRule = RxTestSchedulerRule()

    private lateinit var testSubject: CommentsViewModel

    private val initialState = CommentsState(loading = false)

    private val loadingState = CommentsState(loading = true)

    private val commentsUseCase = mock<CommentsUseCase>()

    private val observer = mock<Observer<CommentsState>>()

    @Before
    fun setUp() {
        testSubject = CommentsViewModel(initialState, commentsUseCase)
        testSubject.observableState.observeForever(observer)
    }

    @Test
    fun `Given comments successfully loaded, when action LoadCommentsWithId is received, then State contains comment`() {

        val commetsList: List<Comments> = emptyList()
        val comment = Comments(1, 1, "test title", "test@email.com", "test body")
        commetsList.plus(comment)

        val successState = CommentsState(loading = false, comments = commetsList)

        whenever(commentsUseCase.getCommentsList(1)).thenReturn(Single.just(commetsList))

        testSubject.dispatch(CommentsAction.LoadCommentsWithId(1))
        testRule.triggerActions()

        inOrder(observer) {
            verify(observer).onChanged(initialState)
            verify(observer).onChanged(loadingState)
            verify(observer).onChanged(successState)
        }

        verifyNoMoreInteractions(observer)
    }

    @Test
    fun `Given comments failed to load, when action LoadCommentsWithId received, then State contains error`() {

        whenever(commentsUseCase.getCommentsList(1)).thenReturn(Single.error(RuntimeException()))
        val errorState = CommentsState(isError = true)

        testSubject.dispatch(CommentsAction.LoadCommentsWithId(1))
        testRule.triggerActions()

        inOrder(observer) {
            verify(observer).onChanged(initialState)
            verify(observer).onChanged(loadingState)
            verify(observer).onChanged(errorState)
        }

        verifyNoMoreInteractions(observer)
    }

}