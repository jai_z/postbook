package com.sample.postbook.postBook

import com.sample.postbook.utils.Utils
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test


class LoginTests {

    @Test
    fun validNumber_CorrectUserIdSimple_ReturnsTrue() {
        assertTrue(Utils.isValidNumber("123"))
    }

    @Test
    fun invalid_UserId_ReturnsFalse() {
        assertFalse(Utils.isValidNumber("abs@email"))
    }




}